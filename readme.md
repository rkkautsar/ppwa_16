# Digilibre
A library management system. Uses VueJS for front-end and Lumen for back-end.

# Installation
```
$ composer install
$ php artisan migrate
$ npm i -g vbuild
$ composer build
$ composer serve
```

# To-do

## Frontend
- [x] Login/Register
- [x] Book List
- [x] Borrowed book list
- [x] Book search
- [x] Book Details
- [x] Book review
- [x] Homepage
- [x] Admin page
  - [x] Admin book list
  - [x] Admin book add
  - [x] Admin book edit
  - [x] Admin book delete

## Backend
- [x] Book API
- [x] Review API
- [x] Login/Register (JWT Auth)
- [x] Validation and error handling
