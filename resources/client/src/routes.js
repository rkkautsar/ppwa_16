import Home from './pages/Home.vue'
import Login from './pages/Login.vue'
import Books from './pages/Books.vue'
import MyBooks from './pages/MyBooks.vue'
import BookDetails from './pages/BookDetails.vue'
import Admin from './pages/Admin.vue'
import NotFound from './pages/NotFound.vue'

export default [
  {
    path: '',
    component: Home
  },
  {
    name: 'index',
    path: '/',
    component: Home
  },
  {
    name: 'login.index',
    path: '/login',
    component: Login,
    meta: {
      guest: true
    }
  },
  {
    name: 'books.index',
    path: '/browse',
    component: Books,
    alias: '/books'
  },
  {
    name: 'books.detail',
    path: '/books/:id',
    component: BookDetails
  },
  {
    name: 'user.books',
    path: '/me/books',
    component: MyBooks,
    alias: '/me',
    meta: {
      auth: true
    }
  },
  {
    name: 'admin.index',
    path: '/admin',
    component: Admin,
    meta: {
      admin: true
    }
  },
  {
    name: '404',
    path: '*',
    component: NotFound
  }
]