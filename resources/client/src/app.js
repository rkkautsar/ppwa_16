/**
 * ES6 Modules
 */
import Vue from 'vue'
import App from 'components/App.vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import VueMaterial from 'vue-material'
import { sync } from 'vuex-router-sync'
import { default as VMediaQuery } from 'v-media-query'

/**
 * ES5 Modules
 */
const VueTruncate = require('vue-truncate-filter')

/**
 * CSS
 */
import 'vue-material/dist/vue-material.css'
import 'flexboxgrid/dist/flexboxgrid.css'
import 'sweetalert2/dist/sweetalert2.css'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueMaterial)
Vue.use(VueTruncate)
Vue.use(VMediaQuery, {
  variables: {
    md: '48rem'
  }
})

import authService from './services/auth'
Vue.http.headers.common.accept = 'application/json'
Vue.http.options.root = '/api'
Vue.http.interceptors.push((request, next) => {
  next((response) => {
    // If the token is invalid
    if (response.status === 401) {
      authService.logout();
    }
  });
})

import routes from './routes'
import store from './store'
import { checkAuth } from './store/modules/auth/actions'

checkAuth(store)

export const router = new VueRouter({
  routes,
  mode: 'history',
  base: '/app/'
})

Vue.material.theme.register('default', {
  primary: 'deep-purple',
  accent: 'pink'
})


// Transition guard
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth) && !store.state.auth.authenticated) {
    next({
      name: 'login.index',
    })
  } else if (to.matched.some(record => record.meta.guest) && store.state.auth.authenticated) {
    next({
      name: 'books.index',
    })
  } else if (to.matched.some(record => record.meta.admin) && !store.getters.isAdmin) {
    next({
      name: 'books.index',
    })
  } else {
    next()
  }
})

sync(store, router)

export default new Vue({
  ...App,
  router,
  store,
})
