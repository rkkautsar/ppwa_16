import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)

/**
 * Modules
 */
import auth from './modules/auth'

export default new Store({

  modules: {
    auth,
  },

  strict: process.env.NODE_ENV !== 'production',
  plugins: process.env.NODE_ENV !== 'production' ? [createLogger()] : [],
})