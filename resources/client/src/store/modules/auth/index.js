import Vue from 'vue'
import decode from 'jwt-decode'

export const state = {
  authenticated: false,
  user: {},
}

export const AT = {
  LOGIN: 'AUTH_LOGIN',
  LOGOUT: 'AUTH_LOGOUT',
  CHECK_AUTH: 'AUTH_CHECK_AUTH',
}

export const mutations = {
  [AT.CHECK_AUTH](state) {
    state.authenticated = !!localStorage.getItem('token')

    if (state.authenticated) {
      const token = localStorage.getItem('token')
      state.user = decode(token)
      Vue.http.headers.common.token = token
    }
  },

  [AT.LOGIN](state, token) {
    localStorage.setItem('token', token)
    state.authenticated = true
    state.user = decode(token)
    Vue.http.headers.common.token = token
  },

  [AT.LOGOUT](state) {
    state.authenticated = false
    state.admin = false
    state.user = {}
    localStorage.removeItem('token')
    Vue.http.headers.common.token = null
  }
}

export const getters = {
  isAdmin: state => !!state.user && state.user.role === 'admin'
}

export default {
  state,
  mutations,
  getters,
}