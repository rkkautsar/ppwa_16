import { AT } from './index'

export const login = ({ commit }, token) => {
  commit(AT.LOGIN, token)
}

export const logout = ({ commit }) => {
  commit(AT.LOGOUT)
}

export const checkAuth = ({ commit }) => {
  commit(AT.CHECK_AUTH)
}
