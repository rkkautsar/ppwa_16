import Vue from 'vue'
import store from '../../store'
import { logout } from '../../store/modules/auth/actions'
import { default as swal } from 'sweetalert2'
import { router } from '../../app'

export default (user) => {
  logout(store)
  swal({
    title: 'See you!',
    type: 'success',
    text: 'Logged out successfully'
  }).then(() => router.go({ name: 'login.index' }))
}