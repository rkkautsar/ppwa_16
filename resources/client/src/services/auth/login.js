import Vue from 'vue'
import store from '../../store'
import { login } from '../../store/modules/auth/actions'
import { default as swal } from 'sweetalert2'
import { router } from '../../app'

const success = ({ data }) => {
  login(store, data.token)
  swal({
    title: 'Success!',
    type: 'success',
    text: 'You\'re logged in!'
  }).then(() => router.go({ name: 'books.index' }))
}

const failed = ({ data }) => {
  swal({
    title: 'Sorry :(',
    type: 'error',
    text: 'Somehow we don\'t recognize you, try again!'
  })
}

export default (user) => {
  Vue.http.post('login', user)
    .then(success, failed)
}