import Vue from 'vue'
import store from '../../store'
import { login } from '../../store/modules/auth/actions'
import { default as swal } from 'sweetalert2'
import { router } from '../../app'

const success = ({ data }) => {
  login(store, data.token)
  swal({
    title: 'You\'re all set!',
    type: 'success',
    text: 'Registered and logged in successfully!'
  }).then(() => router.go({ name: 'books.index' }))
}

const failed = ({ data }) => {
  swal({
    title: 'Sorry :(',
    type: 'error',
    text: 'The server returns this: ' + data.error
  })
}

export default (user) => {
  Vue.http.post('register', user)
    .then(success, failed)
}