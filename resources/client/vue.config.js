module.exports = {
  title: 'Digilibre',
  resolve: true,
  static: {
    from: './static/images',
    to: './'
  },
  dist: '../../public/app',
  publicPath: '/app/',
  compress: false,
}
