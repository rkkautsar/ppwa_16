<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => 'api'], function () use ($app) {
    $app->post('login', 'UserController@login');
    $app->post('register', 'UserController@register');

    $app->get('books', 'BookController@index');
    $app->get('books/{id}', 'BookController@show');

    $app->group(['middleware' => 'auth'], function () use ($app) {
        $app->post('books/{id}/review', 'ReviewController@store');

        $app->get('books/{id}/loan', 'LoanController@show');
        $app->post('books/{id}/loan', 'LoanController@store');
        $app->delete('books/{id}/loan', 'LoanController@destroy');

        $app->get('users/loans', 'LoanController@user');
    });

    $app->group(['middleware' => ['auth', 'admin']], function () use ($app) {
        $app->post('books', 'BookController@store');
        $app->put('books/{id}', 'BookController@update');
        $app->delete('books/{id}', 'BookController@destroy');
    });
});

$app->get('/', function () use ($app) {
    return redirect('app');
});

$app->get('app', function () use ($app) {
    return view('index');
});

$app->get('/app/{any:.*}', function () use ($app) {
return view('index');
});
