<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model {

    protected $table = 'loan';

    public $timestamps = false;

    protected $primaryKey = 'loan_id';

    public function book() {
        return $this->belongsTo('App\Book');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
