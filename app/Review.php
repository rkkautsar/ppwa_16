<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {

    protected $table = 'review';

    public $timestamps = false;

    protected $primaryKey = 'book_id';

    public function book() {
        return $this->belongsTo('App\Book');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
