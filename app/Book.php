<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {

    protected $table = 'book';

    public $timestamps = false;

    protected $primaryKey = 'book_id';

    public function reviews() {
        return $this->hasMany('App\Review');
    }

    public function loans() {
        return $this->hasMany('App\Loan');
    }
}
