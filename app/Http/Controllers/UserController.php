<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController {

    private function generateToken(User $user) {
        $payload = [
            'user_id' => $user->user_id,
            'username' => $user->username,
            'role' => $user->role,
        ];

        $key = getenv('APP_KEY');

        return response()->json([
            'status' => 'ok',
            'token'  => JWT::encode($payload, $key),
        ]);
    }

    public function login(Request $request) {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $username = $request->input('username');
        $password = $request->input('password');

        $user = User::where([
            ['username', '=', $username],
            ['password', '=', $password],
        ])->firstorFail();

        return $this->generateToken($user);
    }

    public function register(Request $request) {
        $this->validate($request, [
            'username' => 'required|unique:user',
            'password' => 'required|confirmed',
        ]);

        $user = new User;

        $user->username = $request->input('username');
        $user->password = $request->input('password');
        $user->role = 'user';

        $user->save();

        return $this->generateToken($user);
    }
}
