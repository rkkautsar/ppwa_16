<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Review;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ReviewController extends BaseController {
    
    public function store(Request $request, $book_id) {
        $this->validate($request, ['content' => 'required']);

        $review = new Review;
        $review->user_id = Auth::user()->user_id;
        $review->book_id = $book_id;
        $review->date = Carbon::now();
        $review->content = $request->content;
        $review->save();

        return response()->json(['status' => 'ok']);
    }
}
