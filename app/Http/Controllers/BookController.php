<?php

namespace App\Http\Controllers;

use App\Book;
use App\User;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class BookController extends BaseController {

    private $rules =  [
        'title'       => 'required',
        'author'      => 'required',
        'publisher'   => 'required',
        'description' => 'required',
        'quantity'    => 'required|integer|min:0',
        'img_path'    => 'required|url',
    ];

    public function index() {
        return response()->json([
            'status' => 'ok',
            'data' => Book::all(),
        ]);
    }

    public function show($id) {
        $book = Book::with('reviews')->findorFail($id);

        foreach ($book->reviews as $review) {
            $review->user = $review->user()->first();
        }

        return response()->json([
            'status' => 'ok',
            'data' => $book
        ]);
    }

    public function store(Request $request) {
        $this->validate($request, $this->rules);
        $query = Book::where('title', $request->title);
        if ($query->count() > 0) {
            $book = $query->first();
            $book->quantity += $request->quantity;
            $book->save();
        } else {
            $book = new Book;
            $book->title = $request->title;
            $book->author = $request->author;
            $book->publisher = $request->publisher;
            $book->description = $request->description;
            $book->quantity = $request->quantity;
            $book->img_path = $request->img_path;
            $book->save();
        }

        return response()->json(['status' => 'ok']);
    }

    public function update(Request $request, $id) {
        $this->validate($request, $this->rules);
        $this->validate($request, ['title' => 'unique:book']);

        $book = Book::findOrFail($id);
        $book->title = $request->title;
        $book->author = $request->author;
        $book->publisher = $request->publisher;
        $book->description = $request->description;
        $book->quantity = $request->quantity;
        $book->img_path = $request->img_path;
        $book->save();

        return response()->json(['status' => 'ok']);
    }

    public function destroy($id) {
        $book = Book::findorFail($id);
        $book->forceDelete();
    }
}
