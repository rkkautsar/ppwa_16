<?php

namespace App\Http\Controllers;

use App\Book;
use App\Loan;
use Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class LoanController extends BaseController {

    private function criteria($book_id) {
        return [
            ['user_id', '=', Auth::user()->user_id],
            ['book_id', '=', $book_id],
        ];
    }

    public function show($book_id) {
        $loan = Loan::where($this->criteria($book_id))->first();

        return response()->json([
            'status' => 'ok',
            'data' => $loan,
        ]);
    }

    public function store($book_id) {
        $book = Book::findOrFail($book_id);

        if (Loan::where($this->criteria($book_id))->count() === 0 && $book->quantity > 0) {
            $book->quantity--;
            $book->save();

            $loan = new Loan;
            $loan->book_id = $book_id;
            $loan->user_id = Auth::user()->user_id;
            $loan->save();

            return response()->json(['status' => 'ok']);
        } else if ($book->quantity > 0) {
            return response()->json([
                'status' => 'error',
                'error' => 'Sorry, you have borrowed this book.',
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'error' => 'Sorry, the stock is empty right now.',
            ], 422);
        }
    }

    public function destroy($book_id) {
        $loan = Loan::where($this->criteria($book_id));
        if ($loan->count() > 0) {
            $loan = $loan->firstOrFail();
            $book = $loan->book;
            $book->quantity++;
            $book->save();

            $loan->forceDelete();
            return response()->json(['status' => 'ok']);
        }

        return response()->json([
            'status' => 'error',
            'error' => 'The book has not borrowed yet!'
        ], 422);
    }

    public function user(){
        $loans = Auth::user()->loans;

        foreach ($loans as $loan) {
            $loan->book = $loan->book()->first();
        }

        return response()->json([
            'status' => 'ok',
            'data' => $loans,
        ]);
    }
}
