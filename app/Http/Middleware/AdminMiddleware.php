<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware {
    public function handle($request, Closure $next){
        if (Auth::check() && Auth::user()->isAdmin()){
            return $next($request);
        }

        $ret = [
            'status' => 'error',
            'error' => '403 Forbidden',
        ];
        return response()->json($ret, 403);
    }
}
