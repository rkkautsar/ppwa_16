<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

    protected $table = 'user';

    protected $hidden = [
        'password', 'role',
    ];

    protected $fillable = [
        'username', 'password',
    ];

    public $timestamps = false;

    protected $primaryKey = 'user_id';

    public function isAdmin() {
        return $this->role === 'admin';
    }

    public function reviews() {
        return $this->hasMany('App\Review');
    }

    public function loans() {
        return $this->hasMany('App\Loan');
    }
}
